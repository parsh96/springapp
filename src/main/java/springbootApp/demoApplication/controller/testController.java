package springbootApp.demoApplication.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.net.InetAddress;


@RestController
public class testController {
@RequestMapping("/hello")
public String home() {
return "Spring boot is working!";
}

@RequestMapping("/scale")
public String scale() {
String  myHost = System.getenv("HOSTNAME");
return "Spring boot is working fine!   " + "Hostname=" + myHost;
}

@GetMapping("/greeting")
public String greeting(@RequestParam(value = "name", defaultValue = "Ashnik") String name,@RequestParam(value = "number", defaultValue = "8097696506") String number) {
	String  myHost = System.getenv("HOSTNAME");
	return "Welcome  " + name + "   to Docker Session   " + "     Hostname= " + myHost;	
}
}
